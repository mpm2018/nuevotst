*** Settings ***
Library     RequestsLibrary

*** Variables ***
${Base-URL}  https://jsonplaceholder.typicode.com
${json}         { "userId": 1,"title": "testing","body": "testing"}

*** Test Cases ***
TC_001_Get_Request
  Create Session  Get_Posts  ${BASE-URL}  verify=True
  ${response}=  get request  Get_Posts  /posts/1
  #log to console  ${response.status_code}
  log to console  ${response.content}
  ${actual_code}=   convert to string  ${response.status_code}
  should be equal  ${actual_code}  200

TC_002_Post_Request
  Create Session  Add_Data  ${BASE-URL}  verify=True
   log to console   ${json}
  &{header}=  create dictionary  Content-type=application/json
  ${response}=  post request  Add_Data  /posts  data=${json}  headers=${header}
  ${actual_code}=   convert to string  ${response.status_code}
  should be equal  ${actual_code}  201

TC_003_Delete_Request
  Create Session  Del_Data  ${BASE-URL}  verify=True
  ${response}=  delete request  Del_Data   /posts/1
  ${actual_code}=   convert to string  ${response.status_code}
  should be equal  ${actual_code}  200

TC_004_Put_Request
  Create Session  Put_Data  ${BASE-URL}  verify=True
  &{header}=  create dictionary  Content-type=application/json
  ${response}=  put request  Put_Data  /posts/1  data=${json}  headers=${header}
  log to console   ${response.content}
  ${actual_code}=   convert to string  ${response.status_code}
  should be equal  ${actual_code}  200
  ${response1}=  get request  Put_Data  /posts/1
  log to console  ${response1.content}